/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umb.santiago.galvis.umb.virtual.commons.database;

import umb.santiago.galvis.umb.virtual.commons.properties.ApplicationProperties;
import umb.santiago.galvis.umb.virtual.commons.properties.DatabaseProperties;
import umb.santiago.galvis.umb.virtual.model.dto.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author samic
 */
public class UConnection {
    private static Connection connection=null;
private static final String ERROR_CREATING_CONNECTION = "Error creando la conexion con la base de datos";
    public static Connection getConnection(){
        try
        {
            if(connection == null)
            {
                Runtime.getRuntime().addShutdownHook(new ShutDownHook());
                DatabaseProperties properties = ApplicationProperties.getDatabaseProperties();
                Class.forName(properties.getDriver());
                String url = properties.getPrefix()+properties.getHost()+":"+properties.getPort()+"/"+properties.getDatabase();
                connection =  DriverManager.getConnection(url,properties.getUser(),properties.getPassword());
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException(ERROR_CREATING_CONNECTION);
        }
        return connection;
    }




}
