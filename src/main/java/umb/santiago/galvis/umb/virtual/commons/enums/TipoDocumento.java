/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umb.santiago.galvis.umb.virtual.commons.enums;

/**
 *
 * @author samic
 */
public enum TipoDocumento {
    CC("Cedula Ciudadania"),
    TI("Tarjeta de Identidad"),
    CE("Cedula de Extranjeria"),
    PP("Pasaporte");
    private final String label;

    private TipoDocumento(String label){
        this.label = label;
    }

    public String getSingleDocument(){
        return this.label;
    }

    public static String[] getAllDocuments(){
        String[]result = new String[TipoDocumento.values().length];
        Integer counter = 0;
        for(TipoDocumento documento:TipoDocumento.values()){
            result[counter]=documento.getSingleDocument();
            counter++;
        }
        return result;
    }

}
  
