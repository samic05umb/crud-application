package umb.santiago.galvis.umb.virtual.controllers;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import umb.santiago.galvis.umb.virtual.commons.database.IUserDAO;
import umb.santiago.galvis.umb.virtual.commons.enums.TipoDocumento;
import umb.santiago.galvis.umb.virtual.model.dto.User;

import java.util.ArrayList;

public class UserController {
    private static IUserDAO dao;
    public UserController(IUserDAO dao){
        this.dao=dao;
    }
    public void showCurrentEntries(JTable table) throws Exception {
        ArrayList<User> users = new ArrayList<>();
        try {
            users = dao.getAllUsers();
        }catch (Exception e){
            throw new Exception("Error obteniendo usuarios para la tabla");
        }
        DefaultTableModel tableModel = (DefaultTableModel)table.getModel();
        if (tableModel.getRowCount() > 0) {
            for (int i = tableModel.getRowCount() - 1; i > -1; i--) {
                tableModel.removeRow(i);
            }
        }
        Object[] rows = new Object[8];
        for(User user:users){
            rows[0]= user.getTipoDocumento();
            rows[1]= user.getNumeroDocumento();
            rows[2]= user.getNombre();
            rows[3]= user.getApellido();
            rows[4]= user.getUser();
            rows[5]= user.getClave();
            rows[6]= user.getCelular();
            rows[7]= user.getCorreoElectronico();
            tableModel.addRow(rows);
        }

    }
    public void setIdTypeIntoCombo(JComboBox<String> comboBox){
        comboBox.setModel(new javax.swing.DefaultComboBoxModel<>(TipoDocumento.getAllDocuments()));
    }

    public void createUserEntry(User user,JTable jTable) throws Exception {
        try{
            dao.updateUser(user);
        }catch (Exception e){
            e.printStackTrace();
            dao.createUser(user);
        }
        showCurrentEntries(jTable);
    }
    public void deleteUser(User user,JTable jTable) throws Exception {
        try {
            dao.deleteUser(user);
        }catch (Exception e){
            throw new Exception("Error eliminando el usuario");
        }
        showCurrentEntries(jTable);

    }
}
