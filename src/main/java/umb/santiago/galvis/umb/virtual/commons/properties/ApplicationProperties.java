package umb.santiago.galvis.umb.virtual.commons.properties;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Map;
import java.util.ResourceBundle;

public class ApplicationProperties {

    private static DatabaseProperties database=null;

    private static Map<String,Map<String,String>> getAllProperties(){
        Yaml yaml = new Yaml();
        InputStream inputStream = ApplicationProperties.class.getClassLoader().getResourceAsStream("application.yml");
        Map<String,Map<String,String>> mapOfProperties = yaml.load(inputStream);
        return mapOfProperties;
    }
    public static DatabaseProperties getDatabaseProperties(){
        if(database==null){
            Map<String,String> properties = getAllProperties().get("database");
            database = new DatabaseProperties();
            database.setUrl(properties.get("url"));
            database.setDriver(properties.get("driver"));
            database.setHost(properties.get("host"));
            database.setPort(properties.get("port"));
            database.setDatabase(properties.get("database"));
            database.setPrefix(properties.get("prefix"));
            database.setUser(properties.get("user"));
            database.setPassword(properties.get("password"));
        }
        return database;

    }
}
