/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umb.santiago.galvis.umb.virtual.commons.database;

import umb.santiago.galvis.umb.virtual.model.dto.User;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author samic
 */
public interface IUserDAO {
    
    public ArrayList<User> getAllUsers() throws SQLException;
    public User getUser(String documentType,String documentNumber) throws SQLException;
    public void updateUser(User user) throws Exception;
    public void deleteUser(User user) throws SQLException;
    public User createUser(User user) throws Exception;
    
}
