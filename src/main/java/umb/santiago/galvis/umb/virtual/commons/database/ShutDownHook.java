/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umb.santiago.galvis.umb.virtual.commons.database;

import java.sql.Connection;

/**
 *
 * @author samic
 */
public class ShutDownHook extends Thread {
    
    @Override
    public void run(){
    try
    {
        Connection connection = UConnection.getConnection();
        connection.close();
    }catch(Exception e)
    {
        e.printStackTrace();
        throw new RuntimeException(e);
    }
    }
}
