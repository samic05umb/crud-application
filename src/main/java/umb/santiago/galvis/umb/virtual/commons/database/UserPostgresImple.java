package umb.santiago.galvis.umb.virtual.commons.database;

import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PSQLException;
import umb.santiago.galvis.umb.virtual.model.dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserPostgresImple implements IUserDAO {
    private static final String GET_ALL_USERS_QUERY = "Select * from crud_app";
    private static final String FIND_A_USER = "select * from crud_app where \"tipodocumento\" = ? and \"numerodocumento\" = ?";
    private static final String CREATE_A_USER = "INSERT INTO public.crud_app " +
            "(tipodocumento, numerodocumento, nombre, appellido, usuario, clave, numerocelular, correoelectronico) " +
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_USER = "UPDATE public.crud_app " +
            "SET tipodocumento=?, numerodocumento=?, nombre=?, appellido=?, usuario=?, clave=?, numerocelular=?, correoelectronico=? WHERE  numerodocumento=? and tipodocumento=?";
    private static final String ERROR_CREANDO_USUARIO = "Error creando el usuario intente mas tarde";
    private static final String DELETE_USER = "DELETE FROM public.crud_app WHERE  numerodocumento=? AND tipodocumento=?";
    @Override
    public ArrayList<User> getAllUsers() throws SQLException {
        ArrayList<User> result = new ArrayList<>();
        Connection con = UConnection.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(GET_ALL_USERS_QUERY);
        User user;
        while(rs.next()){
            user = new User(rs.getString("TipoDocumento"),
                    rs.getString("NumeroDocumento"),
                    rs.getString("Nombre"),
                    rs.getString("Appellido"),
                    rs.getString("Usuario"),
                    rs.getString("Clave"),
                    rs.getString("NumeroCelular"),
                    rs.getString("CorreoElectronico"));
            result.add(user);
        }
        return result;
    }

    @Override
    public User getUser(String documentType, String documentNumber) throws SQLException {
        Connection con = UConnection.getConnection();
        PreparedStatement st = con.prepareStatement(FIND_A_USER);
        User user= null;
        st.setString(1,documentType);
        st.setInt(2,Integer.parseInt(documentNumber));
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            user = new User(rs.getString("TipoDocumento"),
                    Integer.toString(rs.getInt("NumeroDocumento")),
                    rs.getString("Nombre"),
                    rs.getString("Appellido"),
                    rs.getString("Usuario"),
                    rs.getString("Clave"),
                    rs.getString("NumeroCelular"),
                    rs.getString("CorreoElectronico"));
        }
        return user;
    }

    @Override
    public void updateUser(User user) throws Exception {
        User oldUser;
        try {
            oldUser = getUser(user.getTipoDocumento(), user.getNumeroDocumento());
            user = mergeUser(oldUser,user);
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("No current user");
        }
        Connection con = UConnection.getConnection();
        try {
            PreparedStatement statement = con.prepareStatement(UPDATE_USER);
            System.out.println(UPDATE_USER);
            statement.setString(1,user.getTipoDocumento());
            statement.setInt(2,Integer.parseInt(user.getNumeroDocumento()));
            statement.setString(3,user.getNombre());
            statement.setString(4,user.getApellido());
            statement.setString(5,user.getUser());
            statement.setString(6,user.getClave());
            statement.setString(7,user.getCelular());
            statement.setString(8,user.getCorreoElectronico());
            statement.setInt(9,Integer.parseInt(oldUser.getNumeroDocumento()));
            statement.setString(10,oldUser.getTipoDocumento());
            ResultSet rs = statement.executeQuery();
        }catch (PSQLException e){
            if(e.getSQLState()!="02000") {
                e.printStackTrace();
                throw new Exception(ERROR_CREANDO_USUARIO);
            }
        }
    }
    private User mergeUser(User oldUser, User newUser){
        if(!StringUtils.isBlank(newUser.getApellido())){
            oldUser.setApellido(newUser.getApellido());
        }if(!StringUtils.isBlank(newUser.getCelular())){
            oldUser.setCelular(newUser.getCelular());
        } if(!StringUtils.isBlank(newUser.getClave())){
            oldUser.setClave(newUser.getClave());
        } if(!StringUtils.isBlank(newUser.getNombre())){
            oldUser.setNombre(newUser.getNombre());
        } if(!StringUtils.isBlank(newUser.getCorreoElectronico())){
            oldUser.setCorreoElectronico(newUser.getCorreoElectronico());
        } if(!StringUtils.isBlank(newUser.getNumeroDocumento())){
            oldUser.setNumeroDocumento(newUser.getNumeroDocumento());
        } if(!StringUtils.isBlank(newUser.getTipoDocumento())){
            oldUser.setTipoDocumento(newUser.getTipoDocumento());
        } if(!StringUtils.isBlank(newUser.getUser())){
            oldUser.setUser(newUser.getUser());
        }
        return oldUser;
    }

    @Override
    public void deleteUser(User user) throws SQLException {
        Connection con = UConnection.getConnection();
        PreparedStatement st = con.prepareStatement(DELETE_USER);
        st.setInt(1,Integer.parseInt(user.getNumeroDocumento()));
        st.setString(2,user.getTipoDocumento());
        try {
            ResultSet rs = st.executeQuery();
        }catch (PSQLException e){
            e.printStackTrace();
            System.out.println("Ok");
        }
    }

    @Override
    public User createUser(User user) throws Exception {
        Connection con = UConnection.getConnection();
        try {
            PreparedStatement statement = con.prepareStatement(CREATE_A_USER);
            statement.setString(1,user.getTipoDocumento());
            statement.setInt(2,Integer.parseInt(user.getNumeroDocumento()));
            statement.setString(3,user.getNombre());
            statement.setString(4,user.getApellido());
            statement.setString(5,user.getUser());
            statement.setString(6,user.getClave());
            statement.setString(7,user.getCelular());
            statement.setString(8,user.getCorreoElectronico());
            ResultSet rs = statement.executeQuery();
        }catch (PSQLException e){
            if(e.getSQLState()!="02000") {
                throw new Exception(ERROR_CREANDO_USUARIO);
            }
        }
        return user;

    }
}
